
package br.com.senac.ex3;




public class ConversorDeMoedas {
    
    public final double VALOR_DOLLAR = 4.08;
    public final double VALOR_EURO = 4.80;
    public final double VALOR_DOLLAR_AUST = 2.95;
    
    
    public double conversorDollar(double valorReal){
        
        return valorReal/VALOR_DOLLAR;
    }

    public double conversorEuro(double valorReal) {
        return valorReal/VALOR_EURO;
    }

    public double conversorDollarAust(double valorReal) {
        return valorReal/VALOR_DOLLAR_AUST;
    }


    
    
    
}   
