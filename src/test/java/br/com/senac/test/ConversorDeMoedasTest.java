
package br.com.senac.test;

import br.com.senac.ex3.ConversorDeMoedas;
import org.junit.Test;
import static org.junit.Assert.*;


public class ConversorDeMoedasTest {
    
    public ConversorDeMoedasTest() {
        
    }
    
    @Test
    public void quinzeConvertidoEmDollar(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double valorReal = 15;
        double resultado = conversorDeMoedas.conversorDollar(valorReal);
        
        assertEquals(3.67, resultado, 0.05);
   
    }
    @Test
    public void quinzeConvertidoEmEuro(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double valorReal = 15;
        double resultado = conversorDeMoedas.conversorEuro(valorReal);
        
        assertEquals(3.12, resultado, 0.05);
        
    }
    
    @Test
    public void quinzeConvertidoEmDollarAust(){
        ConversorDeMoedas conversorDeMoedas = new ConversorDeMoedas();
        double valorReal = 15;
        double resultado = conversorDeMoedas.conversorDollarAust(valorReal);
        
        assertEquals(5.08, resultado, 0.05);
    }
    
    
    
}
